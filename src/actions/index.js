let nextPerson = 1
export const addName = (name, lName) => ({
  type: 'ADD_NAME',
  id: ++nextPerson,
  name,
  lName,
})

export const updateName = (payload) => ({
  type: 'UPDATE_NAME',
  payload,
})

export const deleteName = (payload) => ({
  type: 'DELETE_NAME',
  payload,
})

export const testSaga = () => ({
  type: 'USER_FETCH_REQUESTED',
})

export const getAllUser = () => ({
  type: 'FETCH_ALL_USER',
})

export const saveAllUser = (users) => ({
  type: 'SAVE_ALL_USER',
  users,
})