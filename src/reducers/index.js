import { combineReducers } from 'redux'
import persons from './persons'
import { reducer as formReducer } from 'redux-form'
import data from './data'

export default combineReducers({
  persons,
  form: formReducer,
  data,
})