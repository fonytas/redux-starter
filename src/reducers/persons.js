const lstPerson = [
  {
    id: 0,
    name: 'fon',
    lName: 'abc',
  },
  {
    id: 1,
    name: 'aaafon',
    lName: 'aaaaabc',
  },
]

const persons = (state = lstPerson, action) => {
  if (action.type === 'ADD_NAME') {
    return [
      ...state,
      {
        id: action.id,
        name: action.name,
        lName: action.lName,
      },
    ]
  }
  else if (action.type === 'UPDATE_NAME') {
    const index = state.findIndex(item => item.id === action.payload.id)
    state[index] = action.payload
    return [...state]

  }
  else if (action.type === 'DELETE_NAME') {
    return state.filter(item => item.id !== action.payload.id)
  }
  return state
}
export default persons