const data = (state = [], action) => {
  if (action.type === 'SAVE_ALL_USER') {
    return action.users
  }
  return state
}
export default data