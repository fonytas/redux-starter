import React from 'react'
import { BrowserRouter as Router, Link, Route } from 'react-router-dom'
import ListAll from './ListAll'
import CreatePerson from './CreatePerson'
import ShowPerson from './ShowPerson'
import BlankForm from './BlankForm'
import AllUsers from './AllUsers'
import EditUsers from './EditUser'

const App = () => (

  <Router>
    {/*<div>*/}
      {/*<nav>*/}
        {/*<li>*/}
          {/*<Link to="/">ListAll</Link>*/}
        {/*</li>*/}
        {/*<li>*/}
          {/*<Link to="/create">Create</Link>*/}
        {/*</li>*/}
      {/*</nav>*/}
    {/*</div>*/}

    <Route path="/" exact component={ListAll} />
    <Route path="/create" exact component={CreatePerson} />
    <Route path="/person/:id" component={ShowPerson} />
    <Route path="/blank" component={BlankForm} />
    <Route path="/allusers" component={AllUsers} />
    <Route path="/edituser/:id" component={EditUsers} />
  </Router>
)

export default App