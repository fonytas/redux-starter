import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { addName } from '../actions'
import { Redirect } from 'react-router-dom'

const CreatePerson = ({ dispatch }) => {
  const [name, setName] = useState("");
  const [lName, setlName] = useState("");
  const [redirect, setRedirect] = useState(false);


  const onChangeName = (event) => {
    setName(event.target.value);
  }

  const onChangelName = (event) => {
    setlName(event.target.value);
  }


  if (redirect) {
    return <Redirect push to="/"/>
  }
  else {
    return (
      <div>
        <form
          onSubmit={e => {
            e.preventDefault();
            if (name === "" || lName === "") {
              return
            }
            dispatch(addName(name, lName))
            setRedirect(true);
          }}
        >
          Name
          <input
            type='text'
            name='name'
            value={name}
            onChange={onChangeName}
          />
          <br />
          LName
          <input
            type='text'
            name='lName'
            value={lName}
            onChange={onChangelName}
          />
          <br />
          <button
            type="submit"
          >
            Create
          </button>
        </form>
      </div>
    )
  }
}

export default connect()(CreatePerson)