import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { FormSection, Field, reduxForm } from 'redux-form'
import { compose } from 'redux'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import get from 'lodash/get'
import set from 'lodash/set'

const validate = values => {
  const errors = {}

  if (!get(values, 'post.title')) {
    set(errors, 'post.title', 'Required')
  }
  if (!values.body) {
    errors.body = ['Required']
  }
  console.log(errors)
  return errors
}

const TextInput = (props) => {
  return (
    <div>
      <TextField
        {...props.input}
        variant='outlined'
        fullWidth
        {...props}
      />
    </div>
  )
}

const EditUser = ({ ...props }) => {
  console.log('props', props)
  return (
    <div style={{ padding: '30px' }}>
      <form
        onSubmit={props.handleSubmit((values) => {
          console.log('values', values)
        })}
      >
        <FormSection name='post'>
          <Field
            name='title'
            type='text'
            component={TextInput}
            label='Title'

          />
        </FormSection>

        <div style={{ paddingTop: '20px' }}>
          <Field
            name='body'
            type='text'
            component={TextInput}
            label='Body'
          />
        </div>
        <Button
          type='submit'
          disabled={props.invalid}
        >Save</Button>
      </form>

    </div>
  )
}

const withForm = reduxForm({ form: 'edit', enableReinitialize: true, validate })
const mapStateToProps = (state, props) => ({
  data: state.data,
  initialValues: {
    post: {
      title: state.data[props.match.params.id - 1]['title'],
    },
    body: state.data[props.match.params.id - 1]['body'],
  },
})
export default compose(
  connect(mapStateToProps),
  withForm,
)(EditUser)