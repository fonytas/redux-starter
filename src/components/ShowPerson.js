import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { deleteName, updateName } from '../actions'

const handleOnSubmit = ({ e, dispatch, data }) => {
  console.log('handleOnSubmit')
  e.preventDefault()
  dispatch(updateName(data))
}

const ShowPerson = ({ persons, dispatch, ...props }) => {
  const id = parseInt(props.match.params.id)
  const selectedPerson = persons.find(p => p.id === +id)
  const [name, setName] = useState('')
  const [lName, setlName] = useState('')

  useEffect( () => {
    if (selectedPerson) {
      setName(selectedPerson.name)
      setlName(selectedPerson.lName)
    }
  }, [])

  const [redirect, setRedirect] = useState(false)

  if (redirect) {
    return <Redirect
      push
      to='/'
    />
  }
  return (
    <div>
      <form
        onSubmit={e => {
          handleOnSubmit({
            e,
            dispatch,
            data: {
              id, name, lName,
            },
          })
          setRedirect(true)
        }}
      >
        Name
        <input
          type='text'
          name='name'
          value={name}
          onChange={({ target }) => setName(target.value)}
        />
        <br />
        LName
        <input
          type='text'
          name='lName'
          value={lName}
          onChange={({ target }) => setlName(target.value)}
        />
        <br />
        <button
          type='submit'
        >
          Update
        </button>
        <button onClick={() => dispatch(deleteName({ id }))}>Delete</button>
      </form>
    </div>
  )

}

const mapStateToProps = state => ({
  persons: state.persons,
})
export default connect(mapStateToProps)(ShowPerson)
