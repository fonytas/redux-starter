import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import { getAllUser } from '../actions'
import { Link } from 'react-router-dom'
import Button from '@material-ui/core/Button'

class AllUsers extends React.Component {

  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    const { dispatch } = this.props
    dispatch(getAllUser())
  }

  render() {
    const { data } = this.props
    if (data.length === 0) {
      return (
        <div>Loading...</div>
      )
    }
    else {
      return (
        <div>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>userId</TableCell>
                <TableCell align='right'>id</TableCell>
                <TableCell align='right'>title</TableCell>
                <TableCell align='right'>body</TableCell>
                <TableCell align='right'>Edit</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map(row => (
                <TableRow key={row.id}>
                  <TableCell
                    component='th'
                    scope='row'
                  >
                    {row.userId}
                  </TableCell>
                  <TableCell align='right'>{row.id}</TableCell>
                  <TableCell align='right'>{row.title}</TableCell>
                  <TableCell align='right'>{row.body}</TableCell>
                  <TableCell align='right'>
                    <Link to={`/edituser/${row.id}`}>
                      <Button variant='outlined'>Edit</Button>
                    </Link>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </div>
      )
    }
  }
}

const mapStateToProps = state => ({
  data: state.data,
})

export default connect(
  mapStateToProps,
)(AllUsers)