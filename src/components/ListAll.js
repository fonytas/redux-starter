import React, { useState, useEffect } from 'react'
import { Redirect, Route, Link } from 'react-router-dom'
import { connect } from 'react-redux'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { getAllUser, testSaga } from '../actions'

const ListAll = ({ persons, dispatch, ...props }) => {

  const [redirect, setRedirect] = useState(false);

  const onClickCreate = () => {
    setRedirect(true);
  }

  const handleClickView = (id) => {
    // console.log(name, lName)
    // console.log("inn", id)
    // console.log('propsprops', props)
    props.history.push(`/person/${id}`)
    // props.history.push(`/person?id=${id}`)
    // props.history.push({pathname: '/person', state: {id: id}})
    // return <Route path="/person"/>
    // props.history.push({pathname: '/person', state: {id: id}})
  }



  if (redirect) {
    return <Redirect push to="/create"/>
  }

  else {
    return (
      <div>
        <button onClick={ () => dispatch(getAllUser())}>TEST</button>
        <button
          type="submit"
          onClick={onClickCreate}
        >
          Create
        </button>
        List all
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell align="right">LName</TableCell>
              <TableCell align="right">View</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {persons.map(person => (
              <TableRow key={person.id}>
                <TableCell component="th" scope="row">
                  {person.name}
                </TableCell>
                <TableCell align="right">{person.lName}</TableCell>
                <TableCell align="right">
                  <Link to={`/person/${person.id}`}>
                    <button >View</button>
                  </Link>
                  {/*<button onClick={() => handleClickView(person.id)}>View</button>*/}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  persons: state.persons
})

export default connect(
  mapStateToProps,
)(ListAll)
