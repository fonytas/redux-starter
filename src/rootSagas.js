import { call, put, takeEvery, takeLatest, select } from 'redux-saga/effects'
import { addName, getAllUser, saveAllUser } from './actions'
import { fetchData } from './api'

function* getApiData(action) {
  try {
    const userData = yield call(fetchData)
    yield put(saveAllUser(userData))
  } catch(e) {

  }
}


function* mySaga() {
  yield takeLatest("FETCH_ALL_USER", getApiData);
}

export default mySaga;